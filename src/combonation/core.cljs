(ns combonation.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [clojure.string :as string]
            [combonation.tricks :as tricks]))

;;
;; ## DB
;;
(def init
  {::combo nil
   ::max-twist 2
   ::max-flip 1
   ::length 3})

;;
;; ## Events
;;
(re-frame/reg-event-fx ::init
  (fn [_ _]
    {:db init
     :dispatch [::rand-combo]}))

(re-frame/reg-event-db ::rand-combo
  (fn [{:keys [::max-twist
               ::max-flip
               ::length]
        :as db}
       _]
    (->> {:max-twist max-twist
          :max-flip max-flip
          :length length}
         tricks/rand-combo
         (assoc db ::combo))))

(defn- update-db [field update-fn]
  (fn [db _]
    (update db field update-fn)))

(defn- inc-until [upper-bound]
  #(if (< % upper-bound)
    (inc %)
    %))

(defn- dec-until [lower-bound]
  #(if (> % lower-bound)
    (dec %)
    %))

(re-frame/reg-event-db ::inc-max-twist
  (update-db ::max-twist (inc-until 5)))

(re-frame/reg-event-db ::dec-max-twist
  (update-db ::max-twist (dec-until 0)))

(re-frame/reg-event-db ::inc-max-flip
  (update-db ::max-flip (inc-until 3)))

(re-frame/reg-event-db ::dec-max-flip
  (update-db ::max-flip (dec-until 0)))

(re-frame/reg-event-db ::inc-length
  (update-db ::length (inc-until 15)))

(re-frame/reg-event-db ::dec-length
  (update-db ::length (dec-until 1)))

;;
;; ## Subs
;;
(re-frame/reg-sub ::combo-in-english
  (fn [db _]
    (some-> db ::combo tricks/combo->english)))

(re-frame/reg-sub ::max-twist ::max-twist)
(re-frame/reg-sub ::max-flip ::max-flip)
(re-frame/reg-sub ::length ::length)
(re-frame/reg-sub ::combo ::combo)

;;
;; ## Views
;;
(defn numeric-control [{:keys [label
                               val-sub
                               inc-sub
                               dec-sub]}]
  (let [val$ (re-frame/subscribe val-sub)]
    (fn []
      [:div.tc
       [:div
        [:div.h2.pt2
         [:span.pointer.ph2.f6.gold.fas.fa-minus
          {:on-click #(re-frame/dispatch dec-sub)}]
         [:div.dib.f5 @val$]
         [:span.pointer.ph2.f6.gold.fas.fa-plus
          {:on-click #(re-frame/dispatch inc-sub)}]]]
        [:div.f5.moon-gray label]])))

(defn control-panel []
  (let [max-twist (re-frame/subscribe [::max-twist]) 
        max-flip (re-frame/subscribe [::max-flip]) 
        length (re-frame/subscribe [::length])]
    (fn []
      ;; Header container
      [:div.w-100.bg-really-dark-gray.pa2.center.flex.items-center.justify-around.shadow-5

       ;; Max-width container
       [:div.w-100.mw6-ns.flex.items-center.justify-around 

        ;; Control length
        [numeric-control
         {:label "length"
          :val-sub [::length]
          :inc-sub [::inc-length]
          :dec-sub [::dec-length]}]

        ;; Control max-flip
        [numeric-control
         {:label "max-flip"
          :val-sub [::max-flip]
          :inc-sub [::inc-max-flip]
          :dec-sub [::dec-max-flip]}]

        ;; Control max-twist
        [numeric-control
         {:label "max-twist"
          :val-sub [::max-twist]
          :inc-sub [::inc-max-twist]
          :dec-sub [::dec-max-twist]}]

        ;; Generate
        [:div.tc.pointer
         {:on-click #(re-frame/dispatch [::rand-combo])}
         [:div.h2.pt2
          [:span.ph2.f4.gold.fas.fa-recycle]]
         [:div.f5.moon-gray "generate"]]]])))

(defn decorated-english-combo []
  (let [english-combo (re-frame/subscribe [::combo-in-english])]
    (fn []
      [:div.f4.ma4.bg-near-black.pv4.ph2.br1.shadow-5.dib
       (mapcat (fn [[t tx]]
                 [^{:key (random-uuid)} [:div.dib.f4.ph2.moon-gray.word-wrap t]
                  (when tx ^{:key (random-uuid)} [:div.dib.f4.ph2.orange.word-wrap tx])])
               (partition 2 2 nil (string/split @english-combo #" ")))])))

(defn select-control [{:keys [label
                              val-tmp
                              val-sub
                              next-sub
                              prev-sub]}]
  (let [#_val$ #_(re-frame/subscribe val-sub)]
    (fn []
      [:div.dib.tc
       [:div
        [:div.ph2
         [:span.pointer.f6.gold.fas.fa-angle-up
          {:on-click #(re-frame/dispatch prev-sub)}]
         [:div.f5 val-tmp]
         [:span.pointer.f6.gold.fas.fa-angle-down
          {:on-click #(re-frame/dispatch next-sub)}]]]
        [:div.f5.pt1.pb2.moon-gray label]])))

(defn transition []
  (fn [tx]
    [:div.mv2.ph2.pt2.bg-really-dark-gray.br1.shadow-5
     [select-control
      {:label "transition"
       :val-tmp (name tx)
       :next-sub [::next-option]
       :prev-sub [::prev-option]}]]))

(defn trick []
  (let [max-twist (re-frame/subscribe [::max-twist]) 
        max-flip (re-frame/subscribe [::max-flip]) 
        length (re-frame/subscribe [::length])]
    (fn [{:keys [takeoff
                 flip
                 twist
                 landing]
          :as parts}]
      (when parts
        (->> [[:takeoff takeoff tricks/takeoffs]
              [:flip    flip    (range (inc @max-flip))]
              [:twist   twist   (range (inc @max-twist))]
              [:landing landing tricks/landings]]
             (map (fn [[label part options]]
                    [select-control
                     {:label label
                      :val-tmp part
                      :next-sub [::next-option]
                      :prev-sub [::prev-option]}]))
             (into [:div.ph2.pt2.bg-really-dark-gray.br1.shadow-5
                    [:div.f4.mb2.moon-gray (tricks/trick->english parts)]]))))))

(defn combo-gui []
  (let [combo (re-frame/subscribe [::combo])]
    (fn []
      (->> @combo
           (partition 2 2 nil)
           (mapcat (fn [[t tx]]
                     (if tx
                       [[(trick) t]
                        [(transition) tx]]
                       [[(trick) t]])))
           (into [:div.flex.flex-column.items-center.justify-around])))))

(defn view []
  ;; Viewport container
   [:div.vw-100.vh-100.overflow-y-scroll.pb5
    [control-panel]
    ;; Max-width container
    [:div.w-100.mw8-ns.tc.center
     [:div.moon-gray.f5.ma2 "(takeoff-flip-twist-landing transition ...)"]
     [decorated-english-combo]
     [combo-gui]]])

;;
;; ## Main
;;
(defn ^:export mount []
  (re-frame/dispatch-sync [::init])
  (enable-console-print!)
  (re-frame/clear-subscription-cache!)
  (js/console.log "Hello, there. ~ Tyler")
  (->> "app"
       js/document.getElementById
       (reagent/render [view])))
