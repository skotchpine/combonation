(ns combonation.tricks
  (:require [clojure.string :as string]
            [clojure.core.match :refer [match]]))

(def flips       [:flipless :single :double :triple])
(def twists      [:twistless :single :double :triple :quad :quint])

(def takeoffs    [:gainer :aerial :raise :cheat])
(def landings    [:complete :hyper :semi :mega])

(def transitions
  (let [swing-tx {:complete [:gainer]
                  :hyper    [:cheat]
                  :semi     [:raise]
                  :mega     [:aerial]}
        punch-tx {:complete [:cheat]
                  :hyper    [:gainer]
                  :semi     [:aerial]
                  :mega     [:raise]}
        carry-tx {:complete [:gainer :aerial]
                  :hyper    [:raise]
                  :semi     [:raise :cheat]
                  :mega     [:gainer]}
        vanish-tx {:complete [:cheat]
                   :hyper    [:gainer :aerial]
                   :semi     [:aerial]
                   :mega     [:raise :cheat]}
        reversal-tx {:complete [:aerial]
                     :hyper    [:raise :cheat]
                     :semi     [:cheat]
                     :mega     [:gainer :aerial]}]
    {:swing    swing-tx
     :carry    carry-tx
     :vanish   vanish-tx
     :reversal reversal-tx
     :rapid    swing-tx
     :skip     vanish-tx
     :pop      carry-tx
     :rpop     carry-tx
     :punch    punch-tx
     :bound    carry-tx}))

(defn rand-takeoff [{:keys [landing
                            transition]}]
  (->> [(or transition (-> transitions keys rand-nth))
        (or landing (rand-nth landings))]
       (get-in transitions)
       rand-nth))

(defn rand-nth-below [coll max-nth]
  (->> coll
       (take (inc max-nth))
       rand-nth))

(def default-max-flip 1)
(defn rand-flip [{:keys [max-flip]}]
  (rand-nth-below flips
                  (or max-flip
                      default-max-flip)))

(def default-max-twist 2)
(defn rand-twist [{:keys [max-twist]}]
  (rand-nth-below twists
                  (or max-twist
                      default-max-twist)))

(defn rand-landing [_]
  (rand-nth landings))

(defn rand-trick
  ([] (rand-trick {}))
  ([opts]
   {:takeoff (rand-takeoff opts)
    :flip    (rand-flip    opts)
    :twist   (rand-twist   opts)
    :landing (rand-landing opts)}))

(def default-length 3)
(defn rand-combo
  ([] (rand-combo {}))
  ([{:keys [length] :as opts}]
   (loop [i (or length default-length)
          combo (list (rand-trick opts))]
     (if (> i 1)
       (let [transition (-> transitions keys rand-nth)
             trick (rand-trick (assoc opts
                                      :transition transition
                                      :landing (-> combo last :landing)))]
         (recur (dec i) (concat combo [transition trick])))
       combo))))

(defn trick->english [{:keys [takeoff
                              flip
                              twist
                              landing]}]
  (match [[takeoff flip twist landing]]
    [[:gainer :single :twistless :complete]] "gainer-switch"
    [[:gainer :single :twistless :hyper]] "gainer"
    [[:gainer :single :twistless :semi]] "gainer-semi"
    [[:gainer :single :twistless :mega]] "gainer-mega"

    [[:gainer :single :single :complete]] "cork"
    [[:gainer :single :single :hyper]] "hypercork"
    [[:gainer :single :single :semi]] "cork-semi"
    [[:gainer :single :single :mega]] "cork-mega"

    [[:gainer :single :double :complete]] "dubcork"
    [[:gainer :single :double :hyper]] "dubcork-hyper"
    [[:gainer :single :double :semi]] "dubcork-semi"
    [[:gainer :single :double :mega]] "dubcork-mega"

    [[:gainer :single :double :complete]] "triplecork"
    [[:gainer :single :double :hyper]] "triplecork-hyper"
    [[:gainer :single :double :semi]] "triplecork-semi"
    [[:gainer :single :double :mega]] "triplecork-mega"

    [[:aerial :single :twistless :complete]] "aerial-switch"
    [[:aerial :single :twistless :hyper]] "aerial"
    [[:aerial :single :twistless :semi]] "aerial-semi"
    [[:aerial :single :twistless :mega]] "aerial-mega"

    [[:aerial :single :single :complete]] "btwist"
    [[:aerial :single :single :hyper]] "hyperbtwist"
    [[:aerial :single :single :semi]] "btwist-semi"
    [[:aerial :single :single :mega]] "btwist-mega"

    [[:aerial :single :double :complete]] "double-btwist"
    [[:aerial :single :double :hyper]] "double-btwist-hyper"
    [[:aerial :single :double :semi]] "double-btwist-semi"
    [[:aerial :single :double :mega]] "double-btwist-mega"

    [[:aerial :single :double :complete]] "triple-btwist"
    [[:aerial :single :double :hyper]] "triple-btwist-hyper"
    [[:aerial :single :double :semi]] "triple-btwist-semi"
    [[:aerial :single :double :mega]] "triple-btwist-mega"

    [[:raise :single :twistless :complete]] "raise"
    [[:raise :single :twistless :hyper]] "sideswipe"
    [[:raise :single :twistless :semi]] "sideswipe-semi"
    [[:raise :single :twistless :mega]] "sideswipe-mega"

    [[:raise :single :single :complete]] "cheat-7-twist"
    [[:raise :single :single :hyper]] "cheat-7-twist-hyper"
    [[:raise :single :single :semi]] "cheat-7-twist-semi"
    [[:raise :single :single :mega]] "cheat-7-twist-mega"

    [[:cheat :single :twistless :complete]] "grand-masterscoot"
    [[:cheat :single :twistless :hyper]] "grand-masterwipe"
    [[:cheat :single :twistless :semi]] "grand-masterswipe-semi"
    [[:cheat :single :twistless :mega]] "grand-masterswipe-mega"

    [[:cheat :single :single :complete]] "tak-full"
    [[:cheat :single :single :hyper]] "tak-full-hyper"
    [[:cheat :single :single :semi]] "tak-full-semi"
    [[:cheat :single :single :mega]] "tak-full-mega"

    :else (->> [takeoff flip twist landing]
               (map name)
               (string/join "-"))))

(defn combo->english [combo]
  (->> combo
       (map #(if (map? %)
               (trick->english %)
               (name %)))
       (string/join " ")))

(comment
  (rand-trick)
  (rand-trick {:max-flip 0
               :max-twist 1})

  (rand-combo)
  (rand-combo {:length 3})

  (trick->english (rand-trick))
  (combo->english (rand-combo))

  (->> {:length 3
        :max-flip 0
        :max-twist 1}
       rand-combo
       combo->english)

  0)
